/*
	bitlash-win32.c: A minimal implementation of certain core Arduino functions	
	
	The author can be reached at: bill@bitlash.net and peter.balogh2@gmail.com

	Copyright (C) 2008-2012 Bill Roy, 2018 Peter Balogh

	Permission is hereby granted, free of charge, to any person
	obtaining a copy of this software and associated documentation
	files (the "Software"), to deal in the Software without
	restriction, including without limitation the rights to use,
	copy, modify, merge, publish, distribute, sublicense, and/or sell
	copies of the Software, and to permit persons to whom theB
	Software is furnished to do so, subject to the following
	conditions:
	
	The above copyright notice and this permission notice shall be
	included in all copies or substantial portions of the Software.
	
	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
	EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
	OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
	NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
	HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
	WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
	FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
	OTHER DEALINGS IN THE SOFTWARE.
*/
#define _CRT_SECURE_NO_WARNINGS

#include <windows.h>
#include <direct.h>
#include "hobbitlash_internal.h"

#define PATH_LEN 256
#define DEFAULT_BITLASH_PATH "/.bitlash/"
extern char bitlash_directory[PATH_LEN];

int main () {
	/*
	FILE *shell = popen("echo ~", "r");
	if (!shell) {;}
	int got = fread(&bitlash_directory, 1, PATH_LEN, shell);
	pclose(shell);
	*/
//	int bytes = GetModuleFileName(NULL, bitlash_directory, PATH_LEN);

	//int got = 0;
	//bitlash_directory[strlen(bitlash_directory) - 1] = 0;	// trim /n
	bitlash_directory[0] = 0;
	//strcat(bitlash_directory, DEFAULT_BITLASH_PATH);

	//sp("Working directory: ");
	//sp(bitlash_directory); speol();

	if (_chdir(bitlash_directory) != 0) {
		sp("Cannot enter .bitlash directory.  Does it exist?\n");
	}

	void init_fake_eeprom(void);
	init_fake_eeprom();

	void init_millis(void);
	init_millis();
	initBitlash(0);

	//signal(SIGINT, inthandler);
	//signal(SIGKILL, inthandler);

//	doCommand((char*)"for (i=0; i<5; i=i+1)\n{\nprint i;\n}");
//	doCommand((char*)"for (;;) { print i; }");

	// run background functions on a separate thread
/*	pthread_create(&background_thread, NULL, BackgroundMacroThread, 0);*/
	// run the main stdin command loop
	for (;;) {
		char * ret = fgets(lbuf, STRVALLEN, stdin);
		if (ret == NULL) break;	
		doCommand(lbuf);
		void initlbuf(void);
		initlbuf();
	}
	
#if 0
	unsigned long next_key_time = 0L;
	unsigned long next_task_time = 0L;

	for (;;) {
		if (millis() > next_key_time) {
//		if (1) {

			// Pipe the serial input into the command handler
			while (serialAvailable()) doCharacter(serialRead());
			next_key_time = millis() + 100L;
		}

		// Background macro handler: feed it one call each time through
		if (millis() > next_task_time) {
			if (!runBackgroundTasks()) next_task_time = millis() + 10L;
		}
	}
#endif

}

/*
	bitlash-win32.c: A minimal implementation of certain core Arduino functions	
	
	The author can be reached at: bill@bitlash.net and peter.balogh2@gmail.com

	Copyright (C) 2008-2012 Bill Roy, 2018 Peter Balogh

	Permission is hereby granted, free of charge, to any person
	obtaining a copy of this software and associated documentation
	files (the "Software"), to deal in the Software without
	restriction, including without limitation the rights to use,
	copy, modify, merge, publish, distribute, sublicense, and/or sell
	copies of the Software, and to permit persons to whom theB
	Software is furnished to do so, subject to the following
	conditions:
	
	The above copyright notice and this permission notice shall be
	included in all copies or substantial portions of the Software.
	
	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
	EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
	OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
	NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
	HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
	WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
	FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
	OTHER DEALINGS IN THE SOFTWARE.
*/
#define _CRT_SECURE_NO_WARNINGS

#include <windows.h>
#include <direct.h>
#include "hobbitlash_internal.h"

#define PATH_LEN 256
char bitlash_directory[PATH_LEN];
#define DEFAULT_BITLASH_PATH "/.bitlash/"

#define CLOCK_REALTIME 0

LARGE_INTEGER getFILETIMEoffset()
{
    SYSTEMTIME s;
    FILETIME f;
    LARGE_INTEGER t;

    s.wYear = 1970;
    s.wMonth = 1;
    s.wDay = 1;
    s.wHour = 0;
    s.wMinute = 0;
    s.wSecond = 0;
    s.wMilliseconds = 0;
    SystemTimeToFileTime(&s, &f);
    t.QuadPart = f.dwHighDateTime;
    t.QuadPart <<= 32;
    t.QuadPart |= f.dwLowDateTime;
    return (t);
}

int
clock_gettime(int X, struct timeval *tv)
{
    LARGE_INTEGER           t;
    FILETIME            f;
    double                  microseconds;
    static LARGE_INTEGER    offset;
    static double           frequencyToMicroseconds;
    static int              initialized = 0;
    static BOOL             usePerformanceCounter = 0;

    if (!initialized) {
        LARGE_INTEGER performanceFrequency;
        initialized = 1;
        usePerformanceCounter = QueryPerformanceFrequency(&performanceFrequency);
        if (usePerformanceCounter) {
            QueryPerformanceCounter(&offset);
            frequencyToMicroseconds = (double)performanceFrequency.QuadPart / 1000000.;
        } else {
            offset = getFILETIMEoffset();
            frequencyToMicroseconds = 10.;
        }
    }
    if (usePerformanceCounter) QueryPerformanceCounter(&t);
    else {
        GetSystemTimeAsFileTime(&f);
        t.QuadPart = f.dwHighDateTime;
        t.QuadPart <<= 32;
        t.QuadPart |= f.dwLowDateTime;
    }

    t.QuadPart -= offset.QuadPart;
    microseconds = (double)t.QuadPart / frequencyToMicroseconds;
    t.QuadPart = microseconds;
    tv->tv_sec = t.QuadPart / 1000000;
    tv->tv_usec = t.QuadPart % 1000000;
    return (0);
}

struct timeval startup_time, current_time, elapsed_time;

// from http://www.guyrutenberg.com/2007/09/22/profiling-code-using-clock_gettime/
struct timeval time_diff(struct timeval start, struct timeval end) {
	struct timeval temp;
	if ((end.tv_usec-start.tv_usec)<0) {
		temp.tv_sec = end.tv_sec-start.tv_sec-1;
		temp.tv_usec = 1000000+end.tv_usec-start.tv_usec;
	} else {
		temp.tv_sec = end.tv_sec-start.tv_sec;
		temp.tv_usec = end.tv_usec-start.tv_usec;
	}
	return temp;
}

void init_millis(void) {
	clock_gettime(CLOCK_REALTIME, &startup_time);
}

unsigned long millis(void) {
	clock_gettime(CLOCK_REALTIME, &current_time);	
	elapsed_time = time_diff(startup_time, current_time);
	uint32_t ret = (elapsed_time.tv_sec * 1000UL) + (elapsed_time.tv_usec / 1000UL);
	return ret;
}


#if 0
// after http://stackoverflow.com/questions/4025891/create-a-function-to-check-for-key-press-in-unix-using-ncurses
//#include <ncurses.h>

int init_keyboard(void) {
	initscr();
	cbreak();
	noecho();
	nodelay(stdscr, TRUE);
	scrollok(stdscr, TRUE);
}

int serialAvailable(void) {
	int ch = getch();

	if (ch != ERR) {
		ungetch(ch);
		return 1;
	} 
	else return 0;
}

int serialRead(void) { return getch(); }
#endif

#if 0
//#include "conio.h"
int lookahead_key = -1;

int serialAvailable(void) { 
	if (lookahead_key != -1) return 1; 
	lookahead_key = mygetch();
	if (lookahead_key == -1) return 0;
	//printf("getch: %d ", lookahead_key);
	return 1;
}

int serialRead(void) {
	if (lookahead_key != -1) {
		int retval = lookahead_key;
		lookahead_key = -1;
		//printf("key: %d", retval);
		return retval;
	}
	return mygetch();
}
#endif

#if 1

int serialAvailable(void) { 
	return 0;
}

int serialRead(void) {
	return '$';
}

#endif
	
void spb (char c) {
	if (serial_override_handler) (*serial_override_handler)(c);
	else {
		putchar(c);
		//printf("%c", c);
		fflush(stdout);
	}
}
void sp(const char *str) { while (*str) spb(*str++); }
void speol(void) { spb(13); spb(10); }

int32_t setBaud(int32_t pin, uint32_t baud) { return 0; }

// stubs for the hardware IO functions
//
unsigned long pins;
void pinMode(byte pin, byte mode) { ; }
int digitalRead(byte pin) { return ((pins & (1<<pin)) != 0); }
void digitalWrite(byte pin, byte value) {
	if (value) pins |= 1<<pin;
	else pins &= ~(1<<pin);
}
int analogRead(byte pin) { return 0; }
void analogWrite(byte pin, byte value) { ; }
int pulseIn(byte pin, byte mode, byte duration) { return 0; }

// stubs for the time functions
//
void delay(unsigned long ms) {
}

void delayMicroseconds(unsigned int us) {
}

// fake eeprom
byte fake_eeprom[E2END];
byte eeread(int addr) { return fake_eeprom[addr]; }
void eewrite(int addr, byte value) { fake_eeprom[addr] = value; }
void init_fake_eeprom(void) {
int i=0;
	while (i <= E2END) eewrite(i++, 0xff);
}

FILE *savefd;
void fputbyte(byte b) {
	fwrite(&b, 1, 1, savefd);	
}


numvar func_save(void) {
	char *fname = (char*) "eeprom";
	if (getarg(0) > 0) fname = (char *) getarg(1);
	savefd = fopen(fname, "w");
	if (!savefd) return 0;
	setOutputHandler(&fputbyte);
//	cmd_ls();
	resetOutputHandler();
	fclose(savefd);
	return 1;
};

numvar func_load(void) {
	FILE *f;
	char *fname;
	if (getarg(0) > 0) fname = (char *)getarg(1);
	else return 0;
	f = fopen(fname, "r");
	if (!f) return 0;
	fseek(f, 0, SEEK_END);
	int len = ftell(f);
	fseek(f, 0, SEEK_SET);
	char *buf = (char*)malloc(len+2);
	fread(buf, 1, len, f);
	buf[len] = 0;
	printf("%s\n", buf);
	doCommand(buf);
	free(buf);
	fclose(f);
	return 1;
};



// background function thread
/* TODO
#include <pthread.h>
pthread_mutex_t executing;
pthread_t background_thread;
struct timespec wait_time;

void *BackgroundMacroThread(void *threadid) {
	for (;;) {
		pthread_mutex_lock(&executing);
		runBackgroundTasks();
		pthread_mutex_unlock(&executing);

		// sleep until next task runtime
		unsigned long sleep_time = millisUntilNextTask();
		if (sleep_time) {
			unsigned long seconds = sleep_time / 1000;
			wait_time.tv_sec = seconds;
			wait_time.tv_nsec = (sleep_time - (seconds * 1000)) * 1000000L;
			while (nanosleep(&wait_time, &wait_time) == -1) continue;
		}
	}
	return 0;
}
*/

numvar func_system(void) {
	return system((char *) getarg(1));
}

numvar func_exit(void) {
	if (getarg(0) > 0) exit(getarg(1));
	exit(0);
}


#include <signal.h>

byte break_received;

void inthandler(int signal) {
	break_received = 1;
}

void addPlatformBitlashFunctions (void)
{
	void addBitlashFunction(const char *, bitlash_function);

	addBitlashFunction("system", (bitlash_function) func_system);
	addBitlashFunction("exit", (bitlash_function) func_exit);
	addBitlashFunction("save", (bitlash_function)func_save);
	addBitlashFunction("load", (bitlash_function)func_load);

	// from bitlash-unix-file.c
	numvar exec(void);
	numvar sdls(void);
	numvar sdexists(void);
	numvar sdrm(void);
	numvar sdcreate(void);
	numvar sdappend(void);
	numvar sdcat(void);
	numvar sdcd(void);
	numvar sdmd(void);
	numvar func_pwd(void);
	//	bitlash_function exec, sdls, sdexists, sdrm, sdcreate, sdappend, sdcat, sdcd, sdmd, func_pwd;
	addBitlashFunction("exec", (bitlash_function) exec);
	addBitlashFunction("dir", (bitlash_function) sdls);
	addBitlashFunction("exists", (bitlash_function) sdexists);
	addBitlashFunction("del", (bitlash_function) sdrm);
//	addBitlashFunction("create", (bitlash_function) sdcreate);
	addBitlashFunction("append", (bitlash_function) sdappend);
	addBitlashFunction("type", (bitlash_function) sdcat);
	addBitlashFunction("cd", (bitlash_function) sdcd);
	addBitlashFunction("md", (bitlash_function) sdmd);
	addBitlashFunction("pwd", (bitlash_function) func_pwd);
	addBitlashFunction("fprintf", (bitlash_function) func_fprintf);
}


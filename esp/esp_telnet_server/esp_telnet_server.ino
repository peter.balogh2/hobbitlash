#ifdef ESP32
#include <WiFi.h>
#else
#include <ESP8266WiFi.h>
#endif
#include "hobbitlashapi.h"

const char* ssid = "WIFI_NAME";
const char* password = "WIFI_PASSWORD";

WiFiServer server(23);
WiFiClient serverClient;

void serialOutputTelnet(byte c)
{
  if (serverClient.connected())
  {
    serverClient.write(&c, 1);
    Serial.printf ("%c", c);
  }
}

numvar func_exit (void)
{
  if (serverClient.connected())
  {
    serverClient.stop(); 
  }
}

void setup() 
{
  initBitlash(115200);
//  Serial.begin(115200);
  WiFi.mode(WIFI_STA);
  WiFi.begin(ssid, password);
  Serial.print("\nConnecting to WiFi: "); Serial.println(ssid);
  uint8_t i = 0;
  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
  }
  //start UART and the server
  server.begin();
  server.setNoDelay(true);
  setOutputHandler(serialOutputTelnet);
  Serial.print("Ready! Use 'telnet ");
  Serial.print(WiFi.localIP());
  Serial.println(" 23' to connect");
  addBitlashFunction("exit", func_exit);
}

const char helloMessage[] = "Welcome to HobbitLash\r\n";

void loop() 
{
  runBitlash();
  //check if there are any new clients
  if (server.hasClient()) 
  {
    if (!serverClient.connected()) 
    {
      if (serverClient) 
      {
        serverClient.stop();
      }
      serverClient = server.available();
      Serial.print("New client: "); Serial.print(serverClient.remoteIP());
      serverClient.write(helloMessage, strlen(helloMessage));
      initBitlash(115200);
/*
      void initTaskList();
      void initlbuf();
      initTaskList();
      initlbuf();*/
    }
    else
    {
      WiFiClient sClient = server.available();
      sClient.stop();
      Serial.println("Connection rejected ");
    }
  }
  //check clients for data
  if (serverClient.connected()) 
  {
    //get data from the telnet client and push it to the UART
    while (serverClient.available()) 
    {
      byte c = serverClient.read();
      if (c==6 || c==3) // CTRL-C
      {
        serverClient.stop();
        Serial.println("Connection terminated");
        break;
      }
      else
      {
        if (c=='\r');
        else doCharacter(c);
      }
    }
  }
}


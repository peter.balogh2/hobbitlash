/***
	bitlash.cpp

	Bitlash is a tiny language interpreter that provides a serial port shell environment
	for bit banging and hardware hacking.

	See the file README for documentation.

	Bitlash lives at: http://bitlash.net
	The author can be reached at: bill@bitlash.net

	Copyright (C) 2008-2012 Bill Roy

	Permission is hereby granted, free of charge, to any person
	obtaining a copy of this software and associated documentation
	files (the "Software"), to deal in the Software without
	restriction, including without limitation the rights to use,
	copy, modify, merge, publish, distribute, sublicense, and/or sell
	copies of the Software, and to permit persons to whom the
	Software is furnished to do so, subject to the following
	conditions:
	
	The above copyright notice and this permission notice shall be
	included in all copies or substantial portions of the Software.
	
	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
	EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
	OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
	NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
	HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
	WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
	FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
	OTHER DEALINGS IN THE SOFTWARE.


***/
#include "src/hobbitlash_internal.h"
#if defined(ARDUINO) && ARDUINO >= 100
  #include "Arduino.h"
#else
//  #include "WProgram.h"
#endif

#ifdef UNIX_BUILD
#include "src/hobbitlash-unix.cpp"
#include "src/hobbitlash-unix-main.cpp"
#include "src/hobbitlash-unix-file.cpp"
#elif defined WIN32_BUILD
#include "src/hobbitlash-win32.cpp"
#include "src/hobbitlash-win32-main.cpp"
#include "src/hobbitlash-win32-file.cpp"
#elif defined ESP_BUILD
#include "src/hobbitlash-esp.cpp"
#include "src/hobbitlash-esp-main.cpp"
#else
#include "src/hobbitlash-arduino.cpp"
#include "src/hobbitlash-arduino-main.cpp"
#endif

#include "src/hobbitlash-cmdline.cpp"
#include "src/hobbitlash-eeprom.cpp"
#include "src/hobbitlash-error.cpp"
#include "src/hobbitlash-functions.cpp"
#include "src/hobbitlash-builtins.cpp"
#include "src/hobbitlash-interpreter.cpp"
#include "src/hobbitlash-instream.cpp"
#include "src/hobbitlash-parser.cpp"
#include "src/hobbitlash-serial.cpp"
#include "src/hobbitlash-taskmgr.cpp"
#include "src/hobbitlash-api.cpp"
#if !defined (UNIX_BUILD) && !defined (WIN32_BUILD)
#include "src/eeprom.cpp"
#endif
